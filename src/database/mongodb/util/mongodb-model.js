import { MongodbConfig } from './mongodb-config';

const DEFAULT_GET_OPTION = {skip: 0, limit: 10, sorts: '-createdAt', lean: true};

export default class MongoDBModel extends MongodbConfig {

  constructor(model, schema) {
    super(model, schema);
  }

  async countDocuments(filters) {
    try {
      const payload = await this.model
      .countDocuments(filters)
      .allowDiskUse(true);
      
      return {payload};
    } catch (error) {
      return {error: error.message};
    }
  }

  async groupBy(groupObjects, query = {}, options = {}) {
    try {
      const payload = await this.model
      .aggregate()
      .group(groupObjects)
      .match(query)
      .sort(options.sort || {createdAt: -1})
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .allowDiskUse(true);
      
      return {payload};
    } catch (error) {
      return {error: error.message};
    }
  }

  async find(query = {}, projection, customOptions = {}) {
    try {
      const options = {...DEFAULT_GET_OPTION, ...customOptions};
      const payload = await this.model.find(query, projection)
      .sort(options.sorts)
      .skip(options.skip || 0)
      .limit(options.limit)
      .lean(customOptions?.lean)
      .allowDiskUse(true);
      return {payload};
    } catch (error) {
      return {error: error.message};
    }
  }

  async findOne(query = {}, projection, customOptions = {}) {
    try {
      const options = {sorts: '-createdAt', ...customOptions};
      const payload = await this.model.findOne(query)
      .select(projection)
      .sort(options.sorts)
      .lean(customOptions?.lean)
      .allowDiskUse(true);
      return {payload};
    } catch (error) {
      return {error: error.message};
    }
  }

  async findById(id, projection, options = {lean: true}) {
    try {
      const payload = await this.model.findById(id, projection, options);
      return {payload};
    } catch (error) {
      return {error: error.message};
    }
  }

  async create(payload) {
    try {
      await this.model.create(payload);
      return {};
    } catch (error) {
      return {error: error.message};
    }
  }

  async insertMany(payload) {
    try {
      await this.model.insertMany(payload);
      return {};
    } catch (error) {
      return {error: error.message};
    }
  }

  async updateOne(filter, payload) {
    try {
      await this.model.updateOne(filter, payload, {new: true, strict: false});
      return {success: true};
    } catch (error) {
      return {error: error.message};
    }
  }

  async updateMany(filter, payload, options) {
    try {
      await this.model.updateMany(filter, payload, options);
      return {success: true};
    } catch (error) {
      return {error: error.message};
    }
  }

  async deleteOne(filters) {
    try {
      await this.model.deleteOne(filters);
      return {success: true};
    } catch (error) {
      return {error: error.message};
    }
  }

  async exists(filter) {
    try {
      return await this.model.exists(filter);
    } catch (error) {
      return false;
    }
  }

  async findByIdAndDelete(id) {
    try {
      await this.model.findByIdAndDelete(id);
      return {};
    } catch (error) {
      return {error: error.message};
    }
  }
  
  async deleteMany(filters) {
    try {
      await this.model.deleteMany(filters, {multi: true});
      return {success: true};
    } catch (error) {
      return {error: error.message};
    }
  }
}