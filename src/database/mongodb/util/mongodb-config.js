'use strict';

import mongoose from 'mongoose';

export class MongodbConfig {
  constructor(modelName, schema) {
    this.modelName = modelName;
    this.model = mongoose.model(this.getCollectionName, schema);
  }
  
  get getCollectionName() {
    let name = this.modelName.replace(/\s+/g, '-');
    name = name.replace(/-/g, '_');
    name = name.toLocaleLowerCase();

    return name;
  }
}