import schema from './system-error.schema';
import MongoDBModel from '../../util/mongodb-model';

class Model extends MongoDBModel {
  constructor() {
    super('system-error', schema);
  }
}

export const SystemErrorModel = new Model();