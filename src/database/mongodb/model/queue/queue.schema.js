const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  metadata: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    enums: ['queued', 'success', 'failed', 'done', 'processing'],
    default: 'queued'
  },
  result: {
    type: String
  }
}, {
  timestamps: true
});

// Indexes
// schema.index({ comment: 1, createdAt: 1  });
// schema.index({ identifier: 1 });

module.exports = schema;