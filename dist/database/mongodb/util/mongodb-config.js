'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports.MongodbConfig = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

class MongodbConfig {
  constructor(modelName, schema) {
    this.modelName = modelName;
    this.model = _mongoose.default.model(this.getCollectionName, schema);
  }

  get getCollectionName() {
    let name = this.modelName.replace(/\s+/g, '-');
    name = name.replace(/-/g, '_');
    name = name.toLocaleLowerCase();

    return name;
  }
}exports.MongodbConfig = MongodbConfig;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsImV4cG9ydHMiLCJ2YWx1ZSIsIk1vbmdvZGJDb25maWciLCJfbW9uZ29vc2UiLCJfaW50ZXJvcFJlcXVpcmVEZWZhdWx0IiwicmVxdWlyZSIsIm9iaiIsIl9fZXNNb2R1bGUiLCJkZWZhdWx0IiwiY29uc3RydWN0b3IiLCJtb2RlbE5hbWUiLCJzY2hlbWEiLCJtb2RlbCIsIm1vbmdvb3NlIiwiZ2V0Q29sbGVjdGlvbk5hbWUiLCJuYW1lIiwicmVwbGFjZSIsInRvTG9jYWxlTG93ZXJDYXNlIl0sInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2RhdGFiYXNlL21vbmdvZGIvdXRpbC9tb25nb2RiLWNvbmZpZy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbmltcG9ydCBtb25nb29zZSBmcm9tICdtb25nb29zZSc7XG5cbmV4cG9ydCBjbGFzcyBNb25nb2RiQ29uZmlnIHtcbiAgY29uc3RydWN0b3IobW9kZWxOYW1lLCBzY2hlbWEpIHtcbiAgICB0aGlzLm1vZGVsTmFtZSA9IG1vZGVsTmFtZTtcbiAgICB0aGlzLm1vZGVsID0gbW9uZ29vc2UubW9kZWwodGhpcy5nZXRDb2xsZWN0aW9uTmFtZSwgc2NoZW1hKTtcbiAgfVxuICBcbiAgZ2V0IGdldENvbGxlY3Rpb25OYW1lKCkge1xuICAgIGxldCBuYW1lID0gdGhpcy5tb2RlbE5hbWUucmVwbGFjZSgvXFxzKy9nLCAnLScpO1xuICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoLy0vZywgJ18nKTtcbiAgICBuYW1lID0gbmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xuXG4gICAgcmV0dXJuIG5hbWU7XG4gIH1cbn0iXSwibWFwcGluZ3MiOiJBQUFBLFlBQVksQ0FBQ0EsTUFBQSxDQUFBQyxjQUFBLENBQUFDLE9BQUEsa0JBQUFDLEtBQUEsVUFBQUQsT0FBQSxDQUFBRSxhQUFBOztBQUViLElBQUFDLFNBQUEsR0FBQUMsc0JBQUEsQ0FBQUMsT0FBQSxjQUFnQyxTQUFBRCx1QkFBQUUsR0FBQSxVQUFBQSxHQUFBLElBQUFBLEdBQUEsQ0FBQUMsVUFBQSxHQUFBRCxHQUFBLEtBQUFFLE9BQUEsRUFBQUYsR0FBQTs7QUFFekIsTUFBTUosYUFBYSxDQUFDO0VBQ3pCTyxXQUFXQSxDQUFDQyxTQUFTLEVBQUVDLE1BQU0sRUFBRTtJQUM3QixJQUFJLENBQUNELFNBQVMsR0FBR0EsU0FBUztJQUMxQixJQUFJLENBQUNFLEtBQUssR0FBR0MsaUJBQVEsQ0FBQ0QsS0FBSyxDQUFDLElBQUksQ0FBQ0UsaUJBQWlCLEVBQUVILE1BQU0sQ0FBQztFQUM3RDs7RUFFQSxJQUFJRyxpQkFBaUJBLENBQUEsRUFBRztJQUN0QixJQUFJQyxJQUFJLEdBQUcsSUFBSSxDQUFDTCxTQUFTLENBQUNNLE9BQU8sQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDO0lBQzlDRCxJQUFJLEdBQUdBLElBQUksQ0FBQ0MsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7SUFDOUJELElBQUksR0FBR0EsSUFBSSxDQUFDRSxpQkFBaUIsQ0FBQyxDQUFDOztJQUUvQixPQUFPRixJQUFJO0VBQ2I7QUFDRixDQUFDZixPQUFBLENBQUFFLGFBQUEsR0FBQUEsYUFBQSIsImlnbm9yZUxpc3QiOltdfQ==